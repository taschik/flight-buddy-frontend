import React from 'react'
import { OverlayView } from "react-google-maps";
import {Jumbotron, Button} from 'react-bootstrap';
import {ListGroup, ListGroupItem} from 'react-bootstrap';
var dateFormat = require('dateformat');

function getPixelPositionOffset(width, height) {
  return { x: width/2, y: 0 };
}

export default class TripOverlayView extends React.Component {
  render() {
    const tripOverlayViewStyle = {
        margin: "20px",
        backgroundColor: "white"
    };

    const jumbotronStyle = {
      borderRadius: 15,
      padding: "20px",
      backgroundColor:'#e1e7f2',
      border: "1px solid #486189"
    }

    const profilePictureStyle = {
      width: 80,
      height: 80,
      backgroundPosition: "center center",
      backgroundRepeat: "no-repeat",
      backgroundImage:  "url(" + this.props.friend.profile_pic + ")" ,
      borderRadius: 40
    }

    console.log(this.props)

    var inboundStartDate = this.props.tripToFriend.departureDate
    var inboundArrivalDate = this.props.tripToFriend.departureDate
    var outboundStartDate = this.props.tripToDestination.departureDate
    var outbundArrivalDate = this.props.tripToDestination.departureDate

    console.log(this.props)

    return (
      <OverlayView
        position={{
          lat: this.props.tripToFriend.destination.location.latitude,
          lng: this.props.tripToFriend.destination.location.longitude
        }}
        getPixelPositionOffset={getPixelPositionOffset}
        mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
      >
      <div style={jumbotronStyle}>
       <img style={profilePictureStyle} src={this.props.friend.picture} />
       <h3>{this.props.friend.name}</h3>
       <h4>Visit {this.props.friend.name} for 2 days and only pay {this.props.priceIncrease}€ extra.</h4>
       <ListGroup>
          <ListGroupItem>
            Leave <span className="bold">{this.props.tripToFriend.start.name}</span> on {inboundStartDate}
          </ListGroupItem>
          <ListGroupItem>
            Arrive in <span className="bold">{this.props.tripToFriend.destination.name}</span> on {inboundArrivalDate}
          </ListGroupItem>
        </ListGroup>
        <ListGroup>
          <ListGroupItem>
            Leave <span className="bold">{this.props.tripToDestination.start.name}</span> on {outboundStartDate}
          </ListGroupItem>
          <ListGroupItem>
            Arrive in <span className="bold">{this.props.tripToDestination.destination.name}</span> on {outbundArrivalDate}
          </ListGroupItem>
       </ListGroup>
       <p><Button href={this.props.tripToDestination.refurl} target="_blank" bsStyle="success">Book now</Button></p>
     </div>
      </OverlayView>
    )
  }

}
